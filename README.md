# inventory-ui

## WIP/TODO

Check out this project's board for features slated for development: 
[https://gitlab.com/nycjv321/inventory-ui/-/boards](https://gitlab.com/nycjv321/inventory-ui/-/boards).


## Preview
![Listing Grid](docs/listing-grid.jpg)

## Backend Setup

### Database

    <INSERT INSTRUCTIONS HERE>

### Service

    <INSERT INSTRUCTIONS HERE>

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
_yarn serve_
```

### Compiles and minifies for production
```
yarn build
```

### Run your unit tests
```
yarn test:unit
```

### Run your end-to-end tests
```
yarn test:e2e
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
