import { expect } from "chai";
import { shallowMount } from "@vue/test-utils";
import GameCardForm from "@/components/GameCardForm.vue";

describe("GameCardForm.vue", () => {
  it("renders props.msg when passed", () => {
    const msg = "new message";
    const wrapper = shallowMount(GameCardForm, {
      propsData: { msg }
    });
    expect(wrapper.text()).to.include(msg);
  });
});
