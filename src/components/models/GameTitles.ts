import GameTitle from "@/components/models/GameTitle";

export default class GameTitles {
  games: GameTitle[];

  constructor(games: GameTitle[]) {
    this.games = games;
  }
}
