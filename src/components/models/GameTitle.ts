export default class GameTitle {
  id: number | null;
  title: string;
  description: string;
  release_date: string;
  genre: string;
  system: string;

  constructor(
    id: number | null,
    title: string,
    description: string,
    release_date: string,
    genre: string,
    system: string
  ) {
    this.id = id;
    this.title = title;
    this.description = description;
    this.release_date = release_date;
    this.genre = genre;
    this.system = system;
  }
}
