export default class NotificationState {
  mode: string;
  message: string;
  displayed: boolean;
  constructor(mode: string, message: string, displayed: boolean) {
    this.mode = mode;
    this.message = message;
    this.displayed = displayed;
  }
}
