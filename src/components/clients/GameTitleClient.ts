import { AxiosResponse, AxiosStatic } from "axios";
import GameTitle from "@/components/models/GameTitle";
import GameSystem from "@/components/models/GameSystem";
import Genre from "@/components/models/Genre";
const axios = require("axios").default;

export class GameTitleClient {
  private axios: AxiosStatic;

  constructor(axios: AxiosStatic) {
    this.axios = axios;
  }

  get(id: Number): Promise<AxiosResponse<GameTitle>> {
    return this.axios.get("http://localhost:8081/games/" + id);
  }

  systems(): Promise<AxiosResponse<GameSystem[]>> {
    return this.axios.get("http://localhost:8081/systems");
  }

  genre(): Promise<AxiosResponse<Genre[]>> {
    return this.axios.get("http://localhost:8081/systems");
  }

  create(gameTitle: GameTitle): Promise<AxiosResponse<GameTitle>> {
    let payload = JSON.stringify(gameTitle);
    return axios.post("http://localhost:8081/games", payload);
  }

  update(gameTitle: GameTitle): Promise<AxiosResponse<GameTitle>> {
    let payload = JSON.stringify(gameTitle);
    return axios.post("http://localhost:8081/games/" + gameTitle.id, payload, {
      headers: { "Content-Type": "application/json" }
    });
  }
}

let Get = new GameTitleClient(axios);

export default Get;
