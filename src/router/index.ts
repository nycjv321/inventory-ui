import Vue from "vue";
import VueRouter from "vue-router";
import GameCardForm from "../views/GameCardForm.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/game",
    name: "add-grid",
    component: GameCardForm
  },
  {
    path: "/games/:id",
    name: "edit-grid",
    component: GameCardForm
  },
  {
    path: "/games",
    name: "list-games",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/GameListingGrid.vue")
  },
  {
    path: "/",
    name: "homepage",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/GameListingGrid.vue")
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
